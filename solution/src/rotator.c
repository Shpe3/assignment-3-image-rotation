#include "rotator.h"
#include "image.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static struct image copy_image(struct image const source) {
  struct image copy = create_image(source.width, source.height);
  if (!copy.data) {
    return (struct image){0};
  }
  memcpy(copy.data, source.data,
         source.width * source.height * sizeof(*source.data));
  return copy;
}

static struct image rotate_left(struct image const source) {
  struct image transformed = create_image(source.height, source.width);
  if (!transformed.data) {
    return (struct image){0};
  }
  for (uint64_t y = 0; y < source.height; ++y) {
    for (uint64_t x = 0; x < source.width; ++x) {
      transformed.data[(transformed.width - y - 1) + x * transformed.width] =
          source.data[x + y * source.width];
    }
  }
  return transformed;
}

static struct image rotate_180(struct image const source) {
  struct image transformed = create_image(source.width, source.height);
  if (!transformed.data) {
    return (struct image){0};
  }
  for (uint64_t y = 0; y < source.height; ++y) {
    for (uint64_t x = 0; x < source.width; ++x) {
      transformed.data[(source.width - x - 1) +
                       (source.height - y - 1) * transformed.width] =
          source.data[x + y * source.width];
    }
  }
  return transformed;
}

static struct image rotate_right(struct image const source) {
  struct image transformed = create_image(source.height, source.width);
  if (!transformed.data) {
    return (struct image){0};
  }
  for (uint64_t y = 0; y < source.height; ++y) {
    for (uint64_t x = 0; x < source.width; ++x) {
      transformed.data[y + (transformed.height - x - 1) * transformed.width] =
          source.data[x + y * source.width];
    }
  }
  return transformed;
}

typedef struct image (*transform_func)(struct image const source);
transform_func transforms[4] = {&copy_image, &rotate_right, &rotate_180,
                                &rotate_left};

struct image transform(struct image src, int64_t angle) {
  angle = (angle % 360 + 360) % 360;
  size_t index = (size_t)(angle / 90);
  return transforms[index](src);
}
