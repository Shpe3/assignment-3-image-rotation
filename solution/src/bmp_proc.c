#include "bmp_proc.h"
#include <stdint.h>
#include <stdio.h>

#define BMP_ALIGN 4
#define BMP_BF_RESERVED 0
#define BMP_BF_HEADER_SIZE 40
#define BMP_BIT_COUNT 24
#define BMP_COLORS_IMPORTANT 0
#define BMP_COLORS_USED 0
#define BMP_COMPRESSION 0
#define BMP_HEADER_PPM 2835
#define BMP_PLANES 1
#define BMP_TYPE 0x4D42

const uint8_t padding_bytes[] = {0, 0, 0, 0};
    
static uint8_t get_padding(uint8_t width) {
  return (BMP_ALIGN - (width * sizeof(struct pixel)) % BMP_ALIGN) % BMP_ALIGN;
}

enum read_status from_bmp(FILE *IN, struct image *img) {
  struct bmp_header header;
  size_t header_size = fread(&header, sizeof(struct bmp_header), 1, IN);
  if (header_size != 1 || header.bfType != BMP_TYPE ||
      header.biBitCount != BMP_BIT_COUNT ||
      header.biCompression != BMP_COMPRESSION) {
    return READ_INVALID_HEADER;
  }

  *img = create_image(header.biWidth, header.biHeight);
  uint8_t *pixel_data = (uint8_t *)img->data;
  size_t data_size = header.biWidth * sizeof(struct pixel);
  for (uint32_t y = 0; y < header.biHeight; ++y) {
    if (fread(pixel_data, data_size, 1, IN) != 1) {
      return READ_INVALID_BITS;
    }
    pixel_data += data_size;
    fseek(IN, get_padding(header.biWidth), SEEK_CUR);
  }
  return READ_OK;
}

enum write_status to_bmp(FILE *OUT, struct image const *img) {
  uint8_t padding_size = get_padding(img->width);
  uint32_t row_padded = img->width * sizeof(struct pixel) + padding_size;
  struct bmp_header header = {.bfType = BMP_TYPE,
                              .bfileSize = row_padded * img->height +
                                           sizeof(struct bmp_header),
                              .bfReserved = BMP_BF_RESERVED,
                              .bOffBits = sizeof(struct bmp_header),
                              .biSize = BMP_BF_HEADER_SIZE,
                              .biWidth = img->width,
                              .biHeight = img->height,
                              .biPlanes = BMP_PLANES,
                              .biBitCount = BMP_BIT_COUNT,
                              .biCompression = BMP_COMPRESSION,
                              .biSizeImage = row_padded * img->height,
                              .biXPelsPerMeter = BMP_HEADER_PPM,
                              .biYPelsPerMeter = BMP_HEADER_PPM,
                              .biClrUsed = BMP_COLORS_USED,
                              .biClrImportant = BMP_COLORS_IMPORTANT};

  if (fwrite(&header, sizeof(struct bmp_header), 1, OUT) != 1) {
    return WRITE_INVALID_HEADER;
  }

  const uint8_t *pixel_data = (const uint8_t *)img->data;
  for (uint32_t y = 0; y < img->height; ++y) {
    if (fwrite(pixel_data, img->width * sizeof(struct pixel), 1, OUT) != 1) {
      return WRITE_INVALID_BITS;
    }
    pixel_data += img->width * sizeof(struct pixel);
    if (fwrite(padding_bytes, padding_size, 1, OUT) != 1) {
      return WRITE_INVALID_BITS;
    }
  }

  return WRITE_OK;
}
