#include <stdio.h>
#include <stdlib.h>

#include "ANSI_colors.h"
#include "bmp_proc.h"
#include "rotator.h"
#include "utils.h"

#define READ_RIGHTS "rb"
#define WRITE_RIGHTS "wb"

void display_error(enum read_status r_stat, enum write_status w_stat);
struct image process_image(const char *src_path, const char *dst_path,
                           const char *angle_str, enum read_status *r_stat,
                           enum write_status *w_stat);

int main(int argc, char *argv[]) {
  if (argc != ARG_NUMBER) {
    fprintf(stderr,
            "%sInvalid usage: the correct usage would be: <source-image> "
            "<rotated-image> <angle>%s\n",
            HCYAN, RESET);
    return WRONG_ARGUMENT_EXIT_CODE;
  }

  enum read_status read_stat = READ_OK;
  enum write_status write_stat = WRITE_OK;
  struct image final_image =
      process_image(argv[SRC_IMG_ARG_NUMBER], argv[DESTINATION_IMG_ARG_NUMBER],
                    argv[ANGLE_ARG_NUMBER], &read_stat, &write_stat);

  if (read_stat != READ_OK || write_stat != WRITE_OK) {
    display_error(read_stat, write_stat);
    delete_image(&final_image);
    return (read_stat != READ_OK) ? READ_ERROR_EXIT_CODE
                                  : WRITE_ERROR_EXIT_CODE;
  }

  printf("%sTransformation complete.%s\n", GREENHB, RESET);
  delete_image(&final_image);
  return 0;
}

void display_error(enum read_status r_stat, enum write_status w_stat) {
  char *read_messages[] = {
      [READ_INVALID_SIGNATURE] =
          REDHB "READ ERROR: invalid BMP header signature." RESET "\n",
      [READ_INVALID_BITS] = REDHB "READ ERROR: invalid BMP bits." RESET "\n",
      [READ_INVALID_HEADER] =
          REDHB "READ ERROR: invalid BMP header." RESET "\n"};

  char *write_messages[] = {[WRITE_INVALID_HEADER] = MAGENTAHB
                            "WRITE ERROR: invalid header." RESET "\n",
                            [WRITE_INVALID_BITS] = MAGENTAHB
                            "WRITE ERROR: invalid bits." RESET "\n"};

  if (r_stat != READ_OK) {
    printf("%s", read_messages[r_stat]);
  } else if (w_stat != WRITE_OK) {
    printf("%s", write_messages[w_stat]);
  }
}

struct image process_image(const char *src_path, const char *dst_path,
                           const char *angle_str, enum read_status *r_stat,
                           enum write_status *w_stat) {
  if (angle_is_valid(angle_str) == 0) {
    fprintf(stderr,
            "%sInvalid angle: only angles divisible by 90 are allowed.%s\n",
            HCYAN, RESET);
    *r_stat = READ_INVALID_BITS;
    return (struct image){0};
  }

  FILE *src_file = fopen(src_path, READ_RIGHTS);
  if (src_file == NULL) {
    printf("%sInvalid file: file not found.%s\n", REDHB, RESET);
    *r_stat = READ_INVALID_SIGNATURE;
    return (struct image){0};
  }

  struct image source;
  *r_stat = from_bmp(src_file, &source);
  fclose(src_file);

  if (*r_stat != READ_OK) {
    return (struct image){0};
  }

  int64_t angle_int = atoi(angle_str);
  struct image destination = transform(source, angle_int);
  FILE *dest_file = fopen(dst_path, WRITE_RIGHTS);
  *w_stat = to_bmp(dest_file, &destination);
  fclose(dest_file);

  delete_image(&source);
  return destination;
}
