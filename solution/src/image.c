#include "image.h"
#include "ANSI_colors.h"
#include "utils.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

bool angle_is_valid(const char *angle) {
  int angle_value = atoi(angle);
  angle_value =
      (angle_value >= 0) ? angle_value % 360 : 360 - (-angle_value % 360);
  switch (angle_value) {
  case 0:
  case 90:
  case 180:
  case 270:
    return true;
  default:
    return false;
  }
}

struct image create_image(uint64_t width, uint64_t height) {
  struct image img = {width, height, NULL};
  img.data = (struct pixel *)calloc(width * height, sizeof(struct pixel));
  if (!img.data) {
    fprintf(stderr, REDHB "ERROR: Out of memory" RESET "\n");
    exit(EXIT_FAILURE);
  }
  return img;
}

void delete_image(struct image *img) {
  free(img->data);
  img->data = NULL;
}
