#ifndef ASSIGNMENT_3_IMAGE_ROTATION_UTILS_H
#define ASSIGNMENT_3_IMAGE_ROTATION_UTILS_H

#include "bmp_proc.h"
#include "image.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

enum error_codes{
    WRONG_ARGUMENT_EXIT_CODE = 1,
    NON_EXISTENT_FILE_EXIT_CODE,
    READ_ERROR_EXIT_CODE,
    WRITE_ERROR_EXIT_CODE,
    ERROR_CODE_OUT_OF_MEMORY
};

enum program_args{
    SRC_IMG_ARG_NUMBER = 1,
    DESTINATION_IMG_ARG_NUMBER,
    ANGLE_ARG_NUMBER,
    ARG_NUMBER
};

bool angle_is_valid(const char* angle);

#endif
