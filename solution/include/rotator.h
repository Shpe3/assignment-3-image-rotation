#ifndef ASSIGNMENT_3_IMAGE_ROTATION_ROTATOR_H
#define ASSIGNMENT_3_IMAGE_ROTATION_ROTATOR_H

#include "image.h"

struct image transform(struct image src, int64_t angle);

#endif
